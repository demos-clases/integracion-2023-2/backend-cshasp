﻿using loginapp.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace loginapp.Tools
{
    public class JwtTool
    {
        public static string ClaveToken = "werewr#$5435dsfdsfdsdwerewr#$5435dsfdsfdsdwerewr#$5435dsfdsfdsd";
        public static int MinutosDeVidaToken = 10;

        public static string CreateToken(DatosUsuarioLogeado usuario)
        {

            // llave simetrica
            var key = Encoding.ASCII.GetBytes(ClaveToken);

            // CREAMOS LOS CLAIMS

            ClaimsIdentity claimsIdentity = new ClaimsIdentity();
            ///////////////////////////////////////////////////////////////////////
            // CONTENIDO DEL TOKEN
            claimsIdentity.AddClaim(new Claim("usuarioId", usuario.UsuarioId.ToString()));
            claimsIdentity.AddClaim(new Claim("usuario", usuario.Usuario));
            claimsIdentity.AddClaim(new Claim("nombres", usuario.Nombres));
            claimsIdentity.AddClaim(new Claim("rol", usuario.Rol));
            ///////////////////////////////////////////////////////////////////////


            var tokenDescriptor = new SecurityTokenDescriptor {
                Subject = claimsIdentity,
                Expires = DateTime.UtcNow.AddMinutes(MinutosDeVidaToken),
                // firma *** cifrado
                SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature
                    )
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            
            var tokenObj = tokenHandler.CreateToken(tokenDescriptor);

            string tokenJwt = tokenHandler.WriteToken(tokenObj);

            return tokenJwt;
        }

        public static DatosUsuarioLogeado DecompilarToken(string token)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(ClaveToken);

            SecurityToken tokenValidado;
            try
            {
                tokenHandler.ValidateToken(
                    token, 
                    new TokenValidationParameters {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ClockSkew = TimeSpan.Zero
                    },
                    out tokenValidado            
                );

            } catch(SecurityTokenExpiredException _) 
            {
                throw new Exception("Token expirado");
            }


            JwtSecurityToken jwtValidado = (JwtSecurityToken)tokenValidado;

            //////////////////////////////////////
            // Obtener lo valores del token
            string id = jwtValidado.Claims.FirstOrDefault(x => x.Type == "usuarioId")?.Value ?? "0";
            string usuario = jwtValidado.Claims.FirstOrDefault(x => x.Type == "usuario")?.Value ?? "";
            string nombres = jwtValidado.Claims.FirstOrDefault(x => x.Type == "nombres")?.Value ?? "";
            string rol = jwtValidado.Claims.FirstOrDefault(x => x.Type == "rol")?.Value ?? "";
            //////////////////
            
            DatosUsuarioLogeado data = new DatosUsuarioLogeado {
                UsuarioId = int.Parse(id),
                Usuario = usuario,
                Nombres = nombres,
                Rol = rol,
            };
            // Obtenemos los claims
            return data;
        }

        public bool VerificarToken(string token)
        {
            return false;
        }
    }
}
