using loginapp.Contexts;
using loginapp.Middlewares;
using loginapp.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(
    options => options.AddDefaultPolicy(
        policy => policy
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    //.WithOrigins("http://localhost:5500")
    )
);

builder.Services.AddDbContext<MiBaseDatosContext>(
    option => option.UseSqlServer(
        @"Data Source=IHR80PBAH22\MSSQLSERVER1;Initial Catalog=demoDb;Integrated Security=True; TrustServerCertificate=True")
);
// Injeccion de dependencias
builder.Services.AddScoped<IUsuarioService, UsuarioServiceImpl>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<JwtUsuarioMiddleware>();

app.Run();
