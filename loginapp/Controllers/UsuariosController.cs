﻿using loginapp.Attributes;
using loginapp.Contexts;
using loginapp.Db.Tables;
using loginapp.Dtos;
using loginapp.Models;
using loginapp.Services;
using loginapp.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace loginapp.Controllers
{
    [ApiController]
    [Route("/api/usuarios")]
    public class UsuariosController : ControllerBase
    {
        private MiBaseDatosContext _db;
        private readonly IUsuarioService _usuarioService;

        public UsuariosController(
            MiBaseDatosContext context,
            IUsuarioService usuarioService
            )
        {
            _db = context;
            _usuarioService = usuarioService;
        }

        

        [HttpPost]
        [Route("")]
        public ActionResult<UsuarioModel> CrearUsuario(
            [FromBody] NuevoUsuarioDto usuario
            )
        {
            _usuarioService.RegistrarUsuario(usuario);
           
            return Ok(usuario);

        }

        
    }
}
