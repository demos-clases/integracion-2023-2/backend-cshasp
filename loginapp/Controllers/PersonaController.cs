﻿using loginapp.Attributes;
using loginapp.Contexts;
using loginapp.Db.Tables;
using loginapp.Models;
using Microsoft.AspNetCore.Mvc;

namespace loginapp.Controllers
{

    [ApiController]
    [Route("/api/personas")]
    public class PersonaController: ControllerBase
    {
        private MiBaseDatosContext _db;

        public PersonaController(MiBaseDatosContext context)
        {
            _db = context;
        }

        [HttpGet]
        [Route("")]
        [IsLogin]
        //[HasRol("superadmin")]
        public ActionResult<List<PersonaModel>> ListarUsuario()
        {

            var personas = _db.Personas.ToList();


            return Ok(personas);

        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<PersonaModel> UsuarioPorID(
            [FromRoute] int id)
        {
            PersonaModel? persona = _db.Personas.Find(id);
            if (persona == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "La persona no existe"
                });
            }
            return Ok(persona);
        }

        [HttpPost]
        [Route("")]
        public ActionResult<PersonaModel> CrearUsuario(
            [FromBody] PersonaModel persona
            )
        {
            _db.Personas.Add(persona);
            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo crear"
                });
            }
            return Ok(persona);

        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<PersonaModel> ActualizarUsuario(
            [FromRoute] int id, [FromBody] PersonaModel personaEditada
            )
        {
            PersonaModel? persona = _db.Personas.Find(id);
            if (persona == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "La persona no existe"
                });
            }

            persona.Nombres = personaEditada.Nombres;
            persona.Apellidos = personaEditada.Apellidos;
            persona.TipoDocumento = personaEditada.TipoDocumento;
            persona.NumeroDocumento = personaEditada.NumeroDocumento;

            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo crear"
                });
            }
            return Ok(persona);

        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult EliminarPorID(
            [FromRoute] int id)
        {
            PersonaModel? persona = _db.Personas.Find(id);



            if (persona == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "La persona no existe"
                });
            }

            _db.Personas.Remove(persona);

            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo eliminar"
                });
            }
            return NoContent();
        }
    }
}
