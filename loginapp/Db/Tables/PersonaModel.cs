﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace loginapp.Db.Tables
{
    [Table("PERSONA")]
    public class PersonaModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("nombre")]
        public string Nombres { get; set; }

        [Column("apellidos")] 
        public string Apellidos { get; set; }

        [Column("tipo_documento")] 
        public string TipoDocumento { get; set; }

        [Column("numero_documento")] 
        public string NumeroDocumento { get; set; }
    }
}
