﻿using loginapp.Models;
using loginapp.Tools;

namespace loginapp.Middlewares
{
    public class JwtUsuarioMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtUsuarioMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var authHeader = (string)context.Request.Headers["Authorization"];
            string? jwt = authHeader?.Split(" ").LastOrDefault();

            if (jwt != null) {
                DatosUsuarioLogeado usuario = JwtTool.DecompilarToken(jwt);

                context.Items["USUARIO"] = usuario;
            }
            await _next(context);
        }
    }
}
