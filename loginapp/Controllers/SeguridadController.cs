﻿using loginapp.Models;
using Microsoft.AspNetCore.Mvc;
using loginapp.Tools;
using loginapp.Services;
using loginapp.Dtos;
using loginapp.Attributes;
using Microsoft.Extensions.Primitives;

namespace loginapp.Controllers
{
    [ApiController]
    [Route("/api/seguridad")]
    public class SeguridadController : ControllerBase
    {
        IUsuarioService _usuarioServ;

        public SeguridadController(IUsuarioService usuarioServ)
        {
            _usuarioServ = usuarioServ;
        }

        [HttpPost]
        [Route("login")]
        public ActionResult< LoginResponse > Login(
            [FromBody] LoginRequest req) {


            var user = _usuarioServ.LoginUsuario(req.Usuario, req.Contrasena);
            if (user == null)
            {
                return NotFound(new CustomErrorResponse {
                    Error = "Usuario o contraseña invalida"
                });
            } else
            {
                DatosUsuarioLogeado du = new DatosUsuarioLogeado
                {
                    Nombres = user.Nombres,
                    Rol = user.Rol,
                    Usuario = user.Usuario,
                    UsuarioId = user.Id
                };
                string token = JwtTool.CreateToken(du);
                LoginResponse res = new LoginResponse
                {
                    Usuario = du,
                    Token = token
                };
                return Ok(res);
            }

        }


        [HttpPut]
        [Route("refresh-token")]
        [IsLogin]
        public ActionResult<LoginResponse> RefreshToken()
        {
            DatosUsuarioLogeado? du = (DatosUsuarioLogeado?)HttpContext.Items["USUARIO"];
            if (du == null) return Unauthorized();

            string token = JwtTool.CreateToken(du);
            LoginResponse res = new LoginResponse
            {
                Usuario = du,
                Token = token
            };
            return Ok(res);
        }

    }
}
