﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace loginapp.Db.Tables
{
    [Table("USUARIOS")]
    public class UsuarioModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("usuario")]
        public string Usuario { get; set; }
        [Column("clave")]
        public string Clave { get; set; }
        [Column("salt")]
        public string Salt { get; set; }
        
        [Column("rol_id")]
        public int RolId { get; set; }
        [Column("persona_id")]
        public int PersonaId { get; set; }
        /*
        [ForeignKey("rol_id")]

        public RolModel Rol { get; set; }
        [ForeignKey("persona_id")]
        public PersonaModel Persona { get; set; }*/
    }
}
