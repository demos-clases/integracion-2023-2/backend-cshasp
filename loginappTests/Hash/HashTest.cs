﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using loginapp.Tools;

namespace loginappTests.Hash
{
    public class HashTest
    {
        string clave1;
        [SetUp]
        public void Setup()
        {
            clave1 = "mimamamemima";
        }

        [Test]
        public void ConvertirCadenaAHashyVerificar()
        {

            string hash = loginapp.Tools.Hash.HashPasword(clave1, out byte[] salt);

            string saltStr = Convert.ToBase64String(salt);

            byte[] salt2 = Convert.FromBase64String(saltStr);

            bool isValid = loginapp.Tools.Hash.VerifyPassword(
                clave1, hash, salt2);

            Assert.IsTrue(isValid);

        }
    }
}
