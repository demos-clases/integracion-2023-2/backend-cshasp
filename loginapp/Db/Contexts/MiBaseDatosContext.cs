﻿using loginapp.Db.Tables;
using Microsoft.EntityFrameworkCore;

namespace loginapp.Contexts
{
    public class MiBaseDatosContext: DbContext
    {
        public MiBaseDatosContext(DbContextOptions<MiBaseDatosContext> options ) 
            : base(options)
        { }


        public DbSet<PersonaModel> Personas { get; set; }
        public DbSet<RolModel> Roles { get; set; }
        public DbSet<UsuarioModel> Usuarios { get; set; }
    }
}
