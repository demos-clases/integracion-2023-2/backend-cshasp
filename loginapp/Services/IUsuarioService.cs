﻿using loginapp.Dtos;

namespace loginapp.Services
{
    public interface IUsuarioService
    {
        public void RegistrarUsuario(NuevoUsuarioDto nuevo);
        public UsuarioDto LoginUsuario(string usuario, string password);
    }
}
