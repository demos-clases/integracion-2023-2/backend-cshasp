﻿using loginapp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace loginapp.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class IsLoginAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            DatosUsuarioLogeado? usuario =
                (DatosUsuarioLogeado?)context.HttpContext.Items["USUARIO"];

            if (usuario == null)
            {
                context.Result = new JsonResult(
                    new CustomErrorResponse
                    {
                        Codigo = "401",
                        Error = "No estas logeado"
                    }
                )
                { StatusCode = 401 };
            }
        }
    }
}
