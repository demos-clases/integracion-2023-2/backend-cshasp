﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace loginapp.Db.Tables
{
    [Table("ROLES")]
    public class RolModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("rol")]
        public string Rol { get; set; }
    }
}
