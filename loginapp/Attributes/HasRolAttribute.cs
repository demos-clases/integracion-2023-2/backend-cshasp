﻿using loginapp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace loginapp.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class HasRolAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string _rol;
        public HasRolAttribute(string rol)
        {
            _rol = rol;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            DatosUsuarioLogeado? usuario =
                (DatosUsuarioLogeado?)context.HttpContext.Items["USUARIO"];

            if (usuario != null && usuario.Rol != _rol)
            {
                context.Result = new JsonResult(
                    new CustomErrorResponse
                    {
                        Codigo = "403",
                        Error = "You should don't pass!!!!"
                    }
                )
                { StatusCode = 403 };
            }
        }
    }
}
