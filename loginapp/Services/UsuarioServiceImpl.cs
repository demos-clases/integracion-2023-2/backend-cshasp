﻿using loginapp.Contexts;
using loginapp.Db.Tables;
using loginapp.Dtos;
using loginapp.Models;
using loginapp.Tools;
using System.Text;

namespace loginapp.Services
{
    public class UsuarioServiceImpl : IUsuarioService
    {

        private MiBaseDatosContext _db;

        public UsuarioServiceImpl(
            MiBaseDatosContext context, 
            ILogger<UsuarioServiceImpl> logger)
        {
            _db = context;
        }

        public UsuarioDto? LoginUsuario(string usuario, string password)
        {
            var exist = _db.Usuarios.Where(us => us.Usuario == usuario).FirstOrDefault();
            if (exist == null) { return null; }
            // comparar la contraseña
            byte[] salt = Convert.FromBase64String(exist.Salt);
            bool esCorrecto = Hash.VerifyPassword(password, exist.Clave, salt);
            if (!esCorrecto) { return null; }


            UsuarioDto? user = _db.Usuarios
                .Where(us => us.Usuario == usuario)
                .Join(
                    _db.Roles,
                    usuario => usuario.RolId,
                    rol => rol.Id,
                    (us, rol) => new { Usuario = us, Rol = rol }
                ).Join(
                    _db.Personas,
                    (us_rol) => us_rol.Usuario.PersonaId,
                    (per) => per.Id,
                    (us_rol, per) => new UsuarioDto
                    {
                        Id = us_rol.Usuario.PersonaId,
                        Nombres = per.Nombres,
                        Apellidos = per.Apellidos,
                        Usuario = us_rol.Usuario.Usuario,
                        Rol = us_rol.Rol.Rol
                    }
                ).FirstOrDefault();
            if (user == null) { return null; }
            else return user;
            
        }

        public void RegistrarUsuario(NuevoUsuarioDto usuario)
        {
            // ANTES DE GUARDAS TENEMOS QUE PROTEGER 
            // LA CLAVE
            string clave_segura = Hash.HashPasword(usuario.Clave, out byte[] salt);
            string salt_str = Convert.ToBase64String(salt);


            PersonaModel per = new PersonaModel
            {
                Nombres = usuario.Nombres,
                Apellidos = usuario.Apellidos,
                TipoDocumento = usuario.TipoDocumento,
                NumeroDocumento = usuario.NumeroDocumento
            };
            _db.Personas.Add(per);
            _db.SaveChanges();
            UsuarioModel usr = new UsuarioModel
            {
                PersonaId = per.Id,
                Usuario = usuario.Usuario,
                Clave = clave_segura,
                RolId = 1,
                Salt = salt_str
            };
            _db.Usuarios.Add(usr);

            int res = _db.SaveChanges();

            if (res < 1)
            {
                throw new Exception("No se pudo registrar el usuario");
            }
        }
    }
}
