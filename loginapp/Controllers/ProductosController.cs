﻿using loginapp.Attributes;
using loginapp.Models;
using Microsoft.AspNetCore.Mvc;

namespace loginapp.Controllers
{
    [ApiController]
    [Route("/api/Productos")]
    public class ProductosController : ControllerBase
    {
        [HttpGet]
        [IsLogin]
        [HasRol("jedi")]
        public ActionResult<IEnumerable<Producto>> ListarProductos()
        {
            List<Producto> productos = new List<Producto>();

            productos.Add( new Producto()
            {
                Id = 1,
                Nombre = "Laptop GAMER ASUS x589",
                Precio = 2586.99m,
                Categoria = "latop,gamer",
            } );

            productos.Add(new Producto()
            {
                Id = 1,
                Nombre = "Silla GAMER 548",
                Precio = 286.99m,
                Categoria = "silla,gamer",
            });

            return Ok(productos);
        }
        
    }
}
