﻿namespace loginapp.Dtos
{
    public class UsuarioDto
    {
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Usuario { get; set; }
        public string? Rol { get; set; }
    }

    public class NuevoUsuarioDto : UsuarioDto
    {
        public string Clave { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
    }

    public class TokenRefreshDto
    {
        public string token { get; set; }
    }
}
