﻿using loginapp.Contexts;
using loginapp.Db.Tables;
using loginapp.Models;
using Microsoft.AspNetCore.Mvc;

namespace loginapp.Controllers
{
    [ApiController]
    [Route("/api/roles")]
    public class RolController: ControllerBase
    {
        private MiBaseDatosContext _db;

        public RolController(MiBaseDatosContext context)
        {
            _db = context;
        }

        [HttpGet]
        [Route("")]
        //[IsLogin]
        //[HasRol("superadmin")]
        public ActionResult<List<RolModel>> ListarRol()
        {

            var roles = _db.Roles.ToList();


            return Ok(roles);

        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<RolModel>RolPorId(
            [FromRoute] int id)
        {
            RolModel? rol = _db.Roles.Find(id);
            if (rol == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "El rol no existe"
                });
            }
            return Ok(rol);
        }

        [HttpPost]
        [Route("")]
        public ActionResult<RolModel> CrearRol(
            [FromBody] RolModel rol
            )
        {
            _db.Roles.Add(rol);
            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo crear"
                });
            }
            return Ok(rol);

        }

        [HttpPut]
        [Route("{id}")]
        public ActionResult<RolModel> ActualizarRol(
            [FromRoute] int id, [FromBody] RolModel rolEditado
            )
        {
            RolModel? rol = _db.Roles.Find(id);
            if (rol == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "El rol no existe"
                });
            }

            rol.Rol = rolEditado.Rol;
            
            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo crear"
                });
            }
            return Ok(rol);

        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult EliminarPorID(
            [FromRoute] int id)
        {
            RolModel? rol = _db.Roles.Find(id);



            if (rol == null)
            {
                return NotFound(new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "El rol no existe"
                });
            }

            _db.Roles.Remove(rol);

            int res = _db.SaveChanges();

            if (res < 1)
            {
                return StatusCode(500, new CustomErrorResponse
                {
                    Codigo = "",
                    Error = "No se pudo eliminar"
                });
            }
            return NoContent();
        }

    }
}
